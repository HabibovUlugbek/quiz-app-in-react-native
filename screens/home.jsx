import React from 'react'
import { StyleSheet, Text, View,Image } from 'react-native'
import { TouchableOpacity } from 'react-native'
import Title from '../components/title'

const Home = () => {
    return (
        <View>
            <Title />
            <View style={styles.bannerContainer}>
                <Image source={{uri:'https://iconscout.com/illustration/qa-service-3678715'}}
                style={styles.banner}
                resizeMode='contain' />
            </View>
            <TouchableOpacity>
                <Text>Start</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    banner:{
        height:300,
        width:300
    },
    bannerContainer:{
        justifyContent:"center",
        alignItems:"center",
    }
})
